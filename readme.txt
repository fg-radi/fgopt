FGopt -- FlightGear Object Placement Tool
-----------------------------------------

(C) 2011 Thomas Albrecht
License: GPLv2

FGopt reads an .stg file, loads the respective objects into FlightGear and lets you adjust the object's properties, such as latitude, longitude, altitude and heading via an easy-to-use GUI. All changes are immediately updated in a seperate FlightGear preview window. Live update is achieved via a telnet connection to a running FlightGear instance.

Written in python, requires wxpython for GUI stuff. It should run on Linux, Mac and Windows. I will continue development as time permits.

Developed/tested on Gentoo Linux, python v2.6.4, wxpython v2.8.10.1.


Installation
============

Linux/Mac
---------
Python should already be installed on your system. You may need to get wxpython, though. Use your distribution's package manager. For example, on Debian/Ubuntu, run:

# sudo apt-get install python-wxgtk2.8

A Mac installer for wxpython is available at http://www.wxpython.org/download.php#stable
Make sure you get the correct version for python2.6.

Assuming you will install version 0.4 in your home directory, unpack the .tgz archive:

# cd
# tar xzf /path/to/download/fgopt-0.4.tgz


Windows (untested)
------------------
- get python from http://www.python.org/download/releases/2.6.6/
- get wxpython from http://www.wxpython.org/download.php#stable
-- TODO --


Usage
=====

1. Run FlightGear with telnet enabled on port 5401:
# fgfs --telnet=5401 --aircraft=ufo

2. Run FGopt:
# ~/fgopt-0.3/bin/fgopt.py [file.stg]


Import objects from file
------------------------
Use File -> Import .stg to add objects to the current list. You may also specify an .stg file at the command line.
The .stg file should *not* already be included in FlightGear's scenery search path; otherwise the respective objects will be displayed twice (no other side-effects, though).


Modify position/orientation of objects
--------------------------------------
You can modify the position of objects by
- entering data directly into list
- use numpad keys or A/D, W/S, Q/E, R/F to move object in 1m steps. Hold down CTRL for 0.1m steps.

Right click focuses object, mouse drag while right clicked changes view.


Local coordinate system
-----------------------
When using numpad keys (or the alternative set of keys), objects are moved with respect to a local coordinate system (LCS). Its origin is set to the position of the first object imported. Heading is set to zero (north). 

A LCS is useful, for example, to move objects parallel to a runway. Just align the heading of the LCS with a runway. Numpad keys will then shift objects parallel/orthogonal to the runway.

The LCS is the first item in the list. You can move it around just like every other object, although there's no visual reference in FG yet. 


Export objects to file
----------------------
Save your work via File -> Export .stg. Currently, this dumps a file named out.stg in current directory.

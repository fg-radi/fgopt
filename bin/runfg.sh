#!/bin/bash
FIND_ELEV_PORT=5401
perfopts="--disable-sound --disable-random-objects --disable-specular-highlight --disable-skyblend --disable-real-weather-fetch --visibility-miles=5 --disable-clouds"
fgfs --telnet=$FIND_ELEV_PORT $perfopts --aircraft=ufo --geometry=640x480 --fg-scenery=$FG_SCENERY --timeofday=noon --airport=LFRB

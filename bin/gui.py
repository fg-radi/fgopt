# --------------------------------------------------------------------------
# Copyright (C) 2011: Thomas Albrecht
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# --------------------------------------------------------------------------

import wx
import sys
import commFG
import coordinates
import copy
import  wx.lib.evtmgr   as  em
import fgopt

import logging
log = logging.getLogger('myapp')

colNum = 0
colType = 1
colPath = 2
colLon = 3
colLat = 4
colAlt = 5
colHdg = 6
#col = 
cols = (colType, colPath, colLon, colLat, colAlt, colHdg)


import  wx.lib.mixins.listctrl  as  listmix

class TestListCtrl(wx.ListCtrl,
                   listmix.TextEditMixin):
#                   listmix.ListCtrlAutoWidthMixin,

    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
#       listmix.ListCtrlAutoWidthMixin.__init__(self)
        listmix.TextEditMixin.__init__(self)


class MyList:
    """filter here"""
    def __init__(self, parent, listWidget):
        self.listWidget = listWidget
        self.listWidget.InsertColumn(colNum , "#")
        self.listWidget.InsertColumn(colType, "Type")
        self.listWidget.InsertColumn(colPath, "Path")
        self.listWidget.InsertColumn(colLon , "Lon")
        self.listWidget.InsertColumn(colLat , "Lat")
        self.listWidget.InsertColumn(colAlt , "Alt")
        self.listWidget.InsertColumn(colHdg , "Hdg")
        self.parent = parent

        self.autoSize = True
        self.objs = []

        self.FG = commFG.commFG()
        self.FG.connect()
        
        parent.transform._observers = [self.FG.updateObject, self.updateListFromObject]
        self.appendObjs([parent.transform])
        self.empty = False

        # we supply a list of observers
        if len(sys.argv) > 1:
            self.importStg(sys.argv[1])
        self.command = self.parent.command
        
        em.eventManager.Register(self.onMotion, wx.EVT_MOTION, self.listWidget)
        em.eventManager.Register(self.onRightDown, wx.EVT_RIGHT_DOWN, self.listWidget)
        #em.eventManager.Register(self.onRightUp, wx.EVT_RIGHT_UP, self.listWidget)
        em.eventManager.Register(self.onKeyDown, wx.EVT_KEY_DOWN, self.listWidget)
        em.eventManager.Register(self.onMouseWheel, wx.EVT_MOUSEWHEEL, self.listWidget)

    def onMotion(self, event):
        ms = wx.GetMouseState()
        if ms.RightDown() and not self.empty:
            log.debug("right down")
            #curPos = event.GetPosition() - self.RightDownPosition
            #self.FG.lookAtObject(self.getCurrent(), \
            #                     radius=50., brg=curPos.x, elevation = 10 - 0.2*curPos.y)
            curPos = event.GetPosition()
            deltaPos = curPos - self.LastDownPosition
            self.FG.changeLookAtPosition(deltaRadius = 0, deltaBrg = deltaPos.x*0.5, deltaElevation = -deltaPos.y*0.5)
            self.LastDownPosition = curPos
            

    def onRightDown(self, event):
        log.debug("right down")
        if self.empty: return
        self.RightDownPosition = event.GetPosition()
        self.LastDownPosition = event.GetPosition()
        self.FG.lookAtObject(self.getCurrent())

    def onMouseWheel(self, event):
        log.debug("Mouse wheel event at %i %i" % (event.X, event.Y))
        log.debug("rot %i" % event.GetWheelRotation())
        if wx.GetMouseState().RightDown() and not self.empty:
            self.FG.changeLookAtPosition(deltaRadius = -event.GetWheelRotation()/12.)
        else:
            event.Skip()

    def onKeyDown(self, event):
        def mychr(keycode):
            if keycode < 256: return chr(keycode)
            else: return False
        keycode = event.GetKeyCode()
        #print "key down", keycode

        if self.empty:
            event.Skip()
            return
        
        if keycode == wx.WXK_INSERT:
            dlg = fgopt.SharedBrowser(self.parent, -1, "This is a Dialog", size=(350, 200))
            dlg.Show()
            event.Skip()
            return
        
        # -- control down gives small increments
        if event.ControlDown():
            incr = 0.1
        else:
            incr = 1.
            
        delta = coordinates.Delta()
        if keycode == wx.WXK_NUMPAD4 or mychr(keycode) == 'A':
            delta.x = -incr
        elif keycode == wx.WXK_NUMPAD6 or mychr(keycode) == 'D':
            delta.x = +incr
        elif keycode == wx.WXK_NUMPAD8 or mychr(keycode) == 'W':
            delta.y = -incr
        elif keycode == wx.WXK_NUMPAD2 or mychr(keycode) == 'S':
            delta.y = +incr
        elif keycode == wx.WXK_NUMPAD_SUBTRACT or mychr(keycode) == 'F':
            delta.alt = -incr
        elif keycode == wx.WXK_NUMPAD_ADD or mychr(keycode) == 'R':
            delta.alt = +incr
        elif keycode == wx.WXK_NUMPAD_MULTIPLY or mychr(keycode) == 'E':
            delta.hdg = -incr * 10
        elif keycode == wx.WXK_NUMPAD_DIVIDE or mychr(keycode) == 'Q':
            delta.hdg = +incr * 10
        else:
            event.Skip()
            return

        obj = self.getCurrent()
        self.command.do(obj.move, delta, obj.move, -delta)
        
    #def onRightUp(self, event):
    #    print "right up"


    def getCurrent(self):
        """return current object"""
        current = self.listWidget.GetFocusedItem()
        if current < 0:
            return None
        return self.objs[current]
        
    def updateListFromObject(self, o):
        """call this if object has changed. We will
           update given object reference in list
        """
        log.debug("update")
        self.setListWidgetItem(self.objs.index(o), o)
    
    def updateObjectFromList(self, event):
        """update object data when list has been edited"""
        log.debug("edit!!!")
        lw = self.listWidget
        #o = self.getCurrent()
        #print self.mylist.listWidget.GetFocusedItem()
        
        #item = lw.GetFirstSelected()
        #itemc = l.GetItem(item, 1.)
        #print "text:", lw.GetItemText(item)
        #self.mylist.updateObject(o)
        #i = lw.GetFocusedItem()
        old = self.objs[event.m_itemIndex]
        new = old.copyWithoutObservers()
        try:
            if   event.m_col == colHdg: new.hdg = float(event.GetText())
            elif event.m_col == colLon: new.lon = float(event.GetText())
            elif event.m_col == colLat: new.lat = float(event.GetText())
            elif event.m_col == colAlt: new.alt = float(event.GetText())
        except ValueError:
            log.error("value error")
            # FIXME: self.updateListFromObject(old) dosnt work here
        else:
            delta = new - old
            self.command.do(old.move, delta, old.move, -delta)
       
    def setListWidgetItem(self, index, o):
        self.listWidget.SetStringItem(index, colNum , str(o.fgidx))
        self.listWidget.SetStringItem(index, colType, o.type)
        self.listWidget.SetStringItem(index, colPath, o.path)
        self.listWidget.SetStringItem(index, colLon , str(o.lon))
        self.listWidget.SetStringItem(index, colLat , str(o.lat))
        self.listWidget.SetStringItem(index, colAlt , str(o.alt))
        self.listWidget.SetStringItem(index, colHdg , str(o.hdg))
#        self.listWidget.SetItemData(index, key)

        
    #def setobjs(self, objs):
    #    self.objs = objs
    #def getobjs(self):
    #    print "getob"
    #    return self.objs
    #self.objs = property(getobjs, setobjs)
    
    def importStg(self, filename):
        """import objects from given filename, append to list"""
        log.debug("Importing " + filename)
        oldlen = len(self.objs)
        self.appendObjs(coordinates.stg(filename, \
                           [self.FG.updateObject, self.updateListFromObject], \
                            self.parent.transform).objs)
        #if len(self.objs) > 0:
        #    self.empty = False
        
        # -- if first import, set local coord system to first object's position
        if oldlen == 1:
            self.parent.transform.setOrigin(self.objs[1].getOrigin())
        
        self.listAutoSize()

    def listAutoSize(self):
        """auto set column width"""
        if self.autoSize:
            self.listWidget.SetColumnWidth(colNum,  wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colType, wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colPath, wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colLon , wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colLat , wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colAlt , wx.LIST_AUTOSIZE)
            self.listWidget.SetColumnWidth(colHdg , wx.LIST_AUTOSIZE)
    
    def appendObjs(self, objs):
        self.objs.extend(objs)
        for o in objs:
            self.FG.updateObject(o)
        self.appendListWidgetItems(objs)
    
    def appendListWidgetItems(self, objs):
        """populate listWidget with given list of objects"""
        
        for o in objs:
            index = self.listWidget.InsertStringItem(sys.maxint, 'bla')
            self.setListWidgetItem(index, o)
            
        self.listWidget.currentItem = 0 # FIXME: for what do we need this?

    def exportStg(self, filename):
        """export objects to out.stg"""
        f = open(filename, "w")
        for o in self.objs[1:]:
            f.write("%s %s %1.8g %1.8g %g %g\n" % (o.type, o.path, o.lon, o.lat, o.alt, o.hdg))
        f.close()
        log.info("wrote %s" % filename)
        

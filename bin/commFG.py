# --------------------------------------------------------------------------
# Copyright (C) 2011: Thomas Albrecht
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# --------------------------------------------------------------------------


#import stgreader
import coordinates
import telnetlib
import sys
import subprocess
import os
import random
import time
import socket

import logging
log = logging.getLogger('myapp')

DEG_PER_M = 1./111000.
FT_PER_M = 1./0.3048

host="localhost"
port=5401

nobj = 0

class commFG:
    """An interface to communicate with FG"""
    def __init__(self):
        self.tn = None
        self.nFGModels = 0
        # view first object. Change to Origin?
        #self.transformation = coordinates.Transformation()
        #self.view = coordinates.Position(None, [self.updateView], 0, 0)
        self.view = coordinates.SphericalRelativePosition(None, [self.updateView])
        print "init", self.view.brg
        self.view.radius = 50.
        
    def connect(self):
        try:
            self.tn = telnetlib.Telnet(host, port)
        except socket.error:
            log.error("could not connect, fall back to stdout.")
            #self.tn = open("fg-telnet.txt", "w")
            self.tn = sys.stdout
        self.tn.write("data\r\n")

    def isFGrunning(self):
        """check if FG is running"""
        return False

    def setProp (self, prop, value):
        """set property to value"""
        if not self.tn: return -1
        self.tn.write("set %s %s\r\n" % (prop, str(value)))
        #log.debug("TN_READ" + self.tn.read_very_lazy())

    def lookAtObject(self, o, radius=50., brg=0., elevation=10.):
        """set viewer position to look at object"""
#        self.view._alt = o.alt * FT_PER_M
        #self.view._hdg = 180.-brg
        #self.view.setSpherical(o, radius, brg, elevation)
        self.view._elevation = elevation
        self.view.other = o
        #self.view._lat = o.lat + 10. * DEG_PER_M
        #self.view.lon = o.lon

    def changeLookAtPosition(self, deltaRadius=0, deltaBrg=0, deltaElevation=0):
        print "changeLookAtPosition", deltaBrg
        if deltaRadius    != 0: self.view.radius    += deltaRadius
        if deltaBrg       != 0: 
            self.view._brg       += deltaBrg
            self.view.hdg = 180.-self.view.brg

        if deltaElevation != 0: self.view.elevation += deltaElevation
        # FIXME: delta method in relposition
        
    def updateView(self, caller):
        self.setProp("/position/longitude-deg", str(caller.lon))
        self.setProp("/position/latitude-deg", str(caller.lat))
        self.setProp("/position/altitude-ft", str(caller.alt * FT_PER_M))
        self.setProp("/orientation/heading-deg", str(caller.hdg))
        self.setProp("/orientation/pitch-deg", str(caller.pit))

        #pipe = subprocess.Popen(["/usr/local/bin/osgviewer", "--window", "800", "0", "400", "300", p])
        #subprocess.Popen(['kill', str(pipe.pid)])

    def updateObject(self, o):
        """send all properties of object to FG. This can create an object, too.
           only react on known object types.
        """
        if not hasattr(o, "fgidx"):
            # -- we do not override model[0] - that's ufo's marker
            self.nFGModels += 1
            o.fgidx = self.nFGModels
            log.info("creating object " + o.path)
        mp="models/model[%i]" % o.fgidx

        # -- prefix full path to static objects
        p = o.path
        if o.type == 'OBJECT_STATIC':
            p = o.stgPath + '/' + p
        elif o.type == 'OBJECT_SHARED':
            pass
        else:
            # FIXME: we could place a marker for local coordinate system
            return

        log.debug("path:" + o.stgPath + '/' + o.path)

        self.setProp (mp+"/path", p)
        self.setProp (mp+"/latitude-deg", str(o.lat))
        self.setProp (mp+"/longitude-deg", str(o.lon))
        self.setProp (mp+"/elevation-ft", str(o.alt * FT_PER_M))
        self.setProp (mp+"/heading-deg", str(-o.hdg))
        self.setProp (mp+"/heading-deg-prop", mp+"/heading-deg")
        self.setProp (mp+"/latitude-deg-prop", mp+"/latitude-deg")
        self.setProp (mp+"/longitude-deg-prop", mp+"/longitude-deg")
        self.setProp (mp+"/elevation-ft-prop", mp+"/elevation-ft")
        self.setProp (mp+"/load", "")
        log.debug("commFG:" + str(o))
        #self.tn.write("\r\n")
        #sys.stdout.write("%s %g %g %g %g\n" % (o.path, o.lat, o.lon, o.alt, o.hdg))
        #print "READ <%s>" % self.tn.read_very_eager()
        #time.sleep(0.1)

def runFG():
    subprocess.Popen(["fgfs"])



def rand(a):
    return random.uniform(-a, a)
    
def place_ok(i):
    mp="models/model[%i]" % i
    setProp (mp+"/path", "Models/Aircraft/747-400-fwa-static-ba.ac")
    setProp (mp+"/latitude-deg", str(48.45128414+rand(500 * DEG_PER_M)))
    setProp (mp+"/longitude-deg", str(-4.403457686 + rand(500 * DEG_PER_M)))
    setProp (mp+"/elevation-ft", str(340 + rand(30)))
    setProp (mp+"/latitude-deg-prop", mp+"/latitude-deg")
    setProp (mp+"/longitude-deg-prop", mp+"/longitude-deg")
    setProp (mp+"/elevation-ft-prop", mp+"/elevation-ft")
    setProp (mp+"/load", "")

    
#heading-deg =   '0'     (double)
#pitch-deg =     '0'     (double)
#roll-deg =      '0'     (double)
#path =  'Models/Agriculture/farmhouse1.ac'      (string)
#latitude-deg =  '48.45214959'   (double)
#latitude-deg-prop =     '/models/model[1]/latitude-deg' (string)
#longitude-deg = '-4.399483821'  (double)
#longitude-deg-prop =    '/models/model[1]/longitude-deg'        (string)
#elevation-ft =  '294.4984487'   (double)
#elevation-ft-prop =     '/models/model[1]/elevation-ft' (string)
#heading-deg-prop =      '/models/model[1]/heading-deg'  (string)
#pitch-deg-prop =        '/models/model[1]/pitch-deg'    (string)
#roll-deg-prop = '/models/model[1]/roll-deg'     (string)


def placeObjects(objs):
    for o in objs:
        #placeObject(o)
        lookAtObject(o)
        time.sleep(0.5)

# -- main
#tn.set_debuglevel(100)
#stg = loadStg(sys.argv[1])
#placeObjects(stg.objs)

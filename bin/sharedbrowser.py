#!/usr/bin/env python
import os,sys

def findFGDataPath():
    """try some common FG data path, return first existing"""
    FG_DATA_PATHS=['C:\\FlightGear', '/usr/share/FlightGear', '/usr/share/games/FlightGear']
    for p in FG_DATA_PATHS:
        if os.path.lexists(p):
            return p
    return None

FG_DATA = findFGDataPath()
                
OBJ_EXTENSIONS = ['xml', 'osg', 'ac']

oldDir = os.getcwdu()
os.chdir(FG_DATA)
def systest():
    for root, dirs, files in os.walk('.'):
        #print root, dirs, files

        # -- prefer .xml to .osg to .ac
        for f in files:
            if f.split('.')[-1] in OBJ_EXTENSIONS:
                print os.path.join(root, f)
os.chdir(olddir)

print "found fg data on", FG_DATA
#systest()

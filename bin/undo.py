#!/usr/bin/env python
# --------------------------------------------------------------------------
# Copyright (C) 2011: Thomas Albrecht
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# --------------------------------------------------------------------------

import logging

log = logging.getLogger('myapp')


class Command:
    """generic command class with undo/redo"""
    def __init__(self):
        self._stack = []
        self._next = 0
        
    def do(self, operation, data, inverseOperation, inverseData = None):
        """call operation(data). If it returns True, record to log.

           A call to undo() later on will call
             inverseOperation(data)
           or
             inverseOperation(inverseData)
           if inverseData is given.
        """
        if operation(data):
            if self._next < len(self._stack):
                # -- invalidate redo: truncate list
                self._stack = self._stack[:self._next]
            self._stack.append((operation, data, inverseOperation, inverseData))
            self._next += 1
            return True
        else:
            log.error("do failed!")
        return False
        
    def undo(self):
        """undo previous command"""
        if self._next == 0:
            log.warning("nothing to undo.")
        else:
            # -- get previous command
            operation, data, inverseOperation, inverseData = self._stack[self._next - 1]
            if inverseData == None: inverseData = data
            if inverseOperation(inverseData):
                self._next -= 1
                return True
        return False
        
    def redo(self):
        """redo next command on stack"""
        if self._next < len(self._stack):
            operation, data, inverseOperation, inverseData = self._stack[self._next]
            if operation(data):
                self._next += 1
                return True
            else:
                return False
        else:
            log.warning("can't redo: no more actions")
            return False

    def showLog(self):
        """show stack"""
        for i in self._stack:
            print ">>", i, "\n"
    

## below is just for testing

# operations
def add((data)):
    a, b = data
    a = a + b
    return True

def addInv(data):
    a, b = data
    a = a + (- b)
    return True

class myint:
    def __init__(self, v):
        self._v = v
    def __add__(self, other):
        self._v += other
    def __float__(self):
        return float(self._v)

if __name__ == "__main__":
    a = myint(1)
    print "init", float(a)
    c = command()
    c.do(add, (a,5), addInv); print "now", float(a)
    c.do(add, (a,3), addInv); print "now", float(a)

    # we recorded two commands, so the third undo complains
    c.undo(); print "undo", float(a)
    c.undo(); print "undo", float(a)
    c.undo(); print "still", float(a)

    # likewise, the third redo complains
    c.redo(); print "redo", float(a)
    c.redo(); print "redo", float(a)
    c.redo(); print "still", float(a)

    # two undo's would allow redo's...
    c.undo(); print "undo", float(a)
    c.undo(); print "undo", float(a)

    # ... but recording a new command will invalidate redo's
    c.do(add, (a,10), addInv); print "now", float(a)
    c.redo(); print "still", float(a)
    
    print "cheating"
    c.do(add, (a,10), add, (a,1)); print "now", float(a)
    c.undo(); print "undo", float(a)
